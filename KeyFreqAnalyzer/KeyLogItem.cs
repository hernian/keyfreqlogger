﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyFreqAnalyzer
{
    class KeyLogItem
    {
        public readonly UInt32 MsSpan;
        public readonly byte KeyChar;

        public KeyLogItem(UInt32 s, byte kc)
        {
            MsSpan = s;
            KeyChar = kc;
        }
    }
}
