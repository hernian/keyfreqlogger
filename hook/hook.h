// 以下の ifdef ブロックは DLL からのエクスポートを容易にするマクロを作成するための 
// より単純に DLL から表示します。この DLL 内のすべてのファイルは、HOOK_EXPORTS を使ってコンパイルされます
// シンボルを使用してコンパイルされます。このシンボルは、この DLL を使用するプロジェクトでは定義できません。
// ソースファイルがこのファイルを含んでいる他のプロジェクトは、 
// HOOK_API 関数は DLL からインポートされているのに対し、この DLL はシンボルを
// シンボルをエクスポートされたと見なします。
#ifdef HOOK_EXPORTS
#define HOOK_API __declspec(dllexport)
#else
#define HOOK_API __declspec(dllimport)
#endif

