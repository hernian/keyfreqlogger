﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KeyFreqLogger
{
    class HookDLL
    {
        public struct KeyLogItem
        {
            public UInt32 SpanKey;
            public byte ScanCode;
            public byte Modifiers;
            public byte Reserved1;
            public byte Reserved2;
        }

        [DllImport("HOOK.dll", EntryPoint = "_SetupHook@8")]
        public static extern UInt32 SetutHook(IntPtr hWndMain, UInt32 uMsdKey);

        [DllImport("HOOK.dll", EntryPoint = "_CleanupHook@0")]
        public static extern void CleanupHook();

        [DllImport("HOOK.dll", EntryPoint = "_SwapBank@0")]
        public static extern UInt32 SwapBank();

        [DllImport("HOOK.dll", EntryPoint = "_GetKeyLogItem@8")]
        public static extern void GetKeyLogItem(UInt32 idx, out KeyLogItem kli);

    }
}
