﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyFreqAnalyzer
{
    class LogReader
    {
        public class Item
        {
            public readonly UInt32 SpanKey;
            public readonly byte ScanCode;
            public readonly byte Modifiers;

            public Item(UInt32 span, byte scan, byte mods)
            {
                SpanKey = span;
                ScanCode = scan;
                Modifiers = mods;
            }
        }

        private const string DIR_LOG = @"c:\keylog";
        private const string FILENAME_PATTERN = "keylog_*.klg";

        private static Encoding _enc = new UTF8Encoding(false);

        public static  Task<List<Item>> ReadAllAsync()
        {
            Func<List<Item>> func = () =>
            {
                var listAll = new List<Item>();
                var fileNames = Directory.EnumerateFiles(DIR_LOG, FILENAME_PATTERN);
                foreach (var fileName in fileNames)
                {
                    var listTemp = ReadFile(fileName);
                    listAll.AddRange(listTemp);
                }
                return listAll;
            };
            return Task.Run(func);
        }

        static List<Item> ReadFile(string fileName)
        {
            var list = new List<Item>();
            using (var reader = new StreamReader(fileName, _enc))
            {
                while (true)
                {
                    var line = reader.ReadLine();
                    if (line == null)
                    {
                        break;
                    }
                    var item = ParseLine(line);
                    list.Add(item);
                }
            }
            return list;
        }

        static Item ParseLine(string line)
        {
            var tokens = line.Split(',');
            if (tokens.Length != 3)
            {
                throw new Exception("行のフォーマット異常");
            }
            var span = Convert.ToUInt32(tokens[0].Trim(), 10);
            var scan = Convert.ToByte(tokens[1].Trim(), 16);
            var mods = Convert.ToByte(tokens[2].Trim(), 16);
            var item = new Item(span, scan, mods);
            return item;
        }
    }
}
