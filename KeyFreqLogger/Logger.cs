﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KeyFreqLogger
{
    class Logger : IDisposable
    {
        private const string DIR_LOG = @"c:\keylog";
        private const string FILENAME_LOG = "keylog.klg";
        private Encoding _enc = new UTF8Encoding(false);
        private Thread _thread;
        private Queue<List<HookDLL.KeyLogItem>> _queue = new Queue<List<HookDLL.KeyLogItem>>();
        private ManualResetEventSlim _mrevent = new ManualResetEventSlim();

        public Logger()
        {
            if (Directory.Exists(DIR_LOG) == false)
            {
                Directory.CreateDirectory(DIR_LOG);
            }

            _thread = new Thread(BackgroundMain);
            _thread.Priority = ThreadPriority.BelowNormal;
            _thread.Start();
        }

        public void Dispose()
        {
            EndThread();
        }

        public void WriteLog()
        {
            var list = GetKeyLogItems();
            Enqueue(list);
        }

        private void BackgroundMain(object arg)
        {
            while (true)
            {
                try
                {
                    var list = Dequeue();
                    if (list == null)
                    {
                        break;
                    }
                    using (var writer = CreateWriter())
                    {
                        WriteList(writer, list);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private List<HookDLL.KeyLogItem> Dequeue()
        {
            var list = default(List<HookDLL.KeyLogItem>);
            _mrevent.Wait();
            lock (_queue)
            {
                _mrevent.Reset();
                if (_queue.Count > 0)
                {
                    list = _queue.Dequeue();
                }
            }
            return list;
        }

        private void Enqueue(List<HookDLL.KeyLogItem> list)
        {
            lock (_queue)
            {
                _queue.Enqueue(list);
                _mrevent.Set();
            }
        }

        private void EndThread()
        {
            lock (_queue)
            {
                _mrevent.Set();
            }
            _thread.Join();
        }

        private TextWriter CreateWriter()
        {
            for (var t = 0; t < 8; ++t)
            {
                try
                {
                    var today = DateTime.Today;
                    var strToday = today.ToString("yyyyMMdd");
                    var fileName = string.Format("keylog_{0}.klg", strToday);
                    var pathName = Path.Combine(DIR_LOG, fileName);
                    var writer = new StreamWriter(pathName, true, _enc);
                    return writer;
                }
                catch (Exception)
                {
                }
                Thread.Sleep(1000);
            }
            throw new Exception("ファイルを開けない");
        }

        private void WriteList(TextWriter writer, List<HookDLL.KeyLogItem> list)
        {
            foreach (var item in list)
            {
                writer.WriteLine("{0}, 0x{1:x}, 0x{2:x}", item.SpanKey, item.ScanCode, item.Modifiers);
            }
        }

        private List<HookDLL.KeyLogItem> GetKeyLogItems()
        {
            var count = (int)HookDLL.SwapBank();
            var list = new List<HookDLL.KeyLogItem>(count);
            for (UInt32 idx = 0; idx < count; ++idx)
            {
                var item = new HookDLL.KeyLogItem();
                HookDLL.GetKeyLogItem(idx, out item);
                list.Add(item);
            }
            return list;
        }
    }
}
