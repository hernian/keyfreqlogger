﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyFreqLogger
{
    public partial class FormMain : Form
    {
        private const UInt16 WM_USER = 0x400;
        private const UInt16 WM_USER_KEY = WM_USER;
        private Logger _logger;

        public FormMain()
        {
            InitializeComponent();
            HookDLL.SetutHook(this.Handle, WM_USER_KEY);
            _logger = new Logger();
        }

        private void WmUserKey(ref Message m)
        {
            // timer1.Interval = 10 * 60 * 1000;           // 10分後
            timer1.Interval = 10 * 1000;           // 10秒後
            timer1.Start();
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_USER_KEY)
            {
                WmUserKey(ref m);
                return;
            }
            base.WndProc(ref m);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            _logger.WriteLog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        protected override void OnClosed(EventArgs e)
        {
            _logger.Dispose();
            base.OnClosed(e);
        }

    }
}
