﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyFreqAnalyzer
{
    class TextBoxMessage
    {
        private TextBox _textBox;

        public TextBoxMessage(TextBox textBox)
        {
            _textBox = textBox;
        }

        public void Clear()
        {
            _textBox.Text = string.Empty;
        }

        public void WriteLine(string msg)
        {
            AppendText(msg);
            AppendText("\r\n");
            _textBox.ScrollToCaret();
        }

        public void WriteLine(string format, params object[] args)
        {
            string msg = string.Format(format, args);
            AppendText(msg);
            AppendText("\r\n");
            _textBox.ScrollToCaret();
        }

        public void Write(string msg)
        {
            AppendText(msg);
            _textBox.ScrollToCaret();
        }

        public void Write(string format, params object[] args)
        {
            var msg = string.Format(format, args);
            AppendText(msg);
            _textBox.ScrollToCaret();
        }

        private void AppendText(string msg)
        {
            var lenPre = _textBox.TextLength;
            _textBox.Select(lenPre, 0);
            _textBox.SelectedText = msg;
            var lenPost = _textBox.TextLength;
            _textBox.Select(lenPost, 0);
        }
    }
}
