﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyFreqAnalyzer
{
    public partial class Form1 : Form
    {
        private TextBoxMessage _message;

        public Form1()
        {
            InitializeComponent();
            _message = new TextBoxMessage(textBoxMsg);
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            _message.Clear();

            var listSymbols = await GetKeyLogItemsAsync();
            var countSymbols = Math.Min(listSymbols.Count, 10);
            foreach (var kc in listSymbols.Take(countSymbols))
            {
                _message.WriteLine("{0:x4} {1}", kc.KeyChar, kc.Count);
            }
        }

        private async Task<List<Analyzer.KeyCount>> GetKeyLogItemsAsync()
        {
            var listSrc = await LogReader.ReadAllAsync().ConfigureAwait(false);
            var listItems = await KeyConv.ConvertToKeyCharAsync(listSrc).ConfigureAwait(false);
            var listSymbols = await Analyzer.GetFreqSymbolsAsync(listItems).ConfigureAwait(false);
            return listSymbols;
        }
    }
}
