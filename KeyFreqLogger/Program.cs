﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace KeyFreqLogger
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool created = false;
            var mutex = new Mutex(true, "KeyFreqLogger$mutex", out created);
            if (created == false)
            {
                return;
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (var form = new FormMain())
            {
                Application.Run();
                form.Close();
            }
            mutex.ReleaseMutex();
        }
    }
}
