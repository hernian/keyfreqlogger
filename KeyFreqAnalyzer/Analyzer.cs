﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyFreqAnalyzer
{
    class Analyzer
    {
        public class KeyCount {
            public readonly UInt32 KeyChar;
            public readonly Int32 Count;

            public KeyCount(UInt32 kc, Int32 cnt)
            {
                KeyChar = kc;
                Count = cnt;
            }
        }

        public static Task<List<KeyCount>> GetFreqAsync(IEnumerable<KeyLogItem> src)
        {
            Func<List<KeyCount>> func = () =>
            {
                return CountKeys(src, item => true);
            };
            return Task.Run(func);
        }

        public static Task<List<KeyCount>> GetFreqSymbolsAsync(IEnumerable<KeyLogItem> src)
        {
            Func<List<KeyCount>> func = () =>
            {
                return CountKeys(src, item => IsSimbol(item.KeyChar));
            };
            return Task.Run(func);
        }

        private static List<KeyCount> CountKeys(IEnumerable<KeyLogItem> src, Predicate<KeyLogItem> predicate)
        {
            var dict = new Dictionary<UInt32, Int32>();
            foreach (var item in src)
            {
                System.Diagnostics.Debug.WriteLine("{0:x4} {1}", item.KeyChar, (char)item.KeyChar);
                if (predicate(item) == false)
                {
                    continue;
                }
                if (dict.ContainsKey(item.KeyChar) == false)
                {
                    dict[item.KeyChar] = 0;
                }
                ++dict[item.KeyChar];
            }
            var listResut = new List<KeyCount>(dict.Count);
            foreach (var kv in dict)
            {
                var keyCount = new KeyCount(kv.Key, kv.Value);
                listResut.Add(keyCount);
            }
            listResut.Sort((a, b) => { return b.Count - a.Count; });
            return listResut;
        }

        private static bool IsSimbol(byte keyChar)
        {
            if ((0x21 <= keyChar) && (keyChar < 0x30))
            {
                return true;
            }
            if ((0x3a <= keyChar) && (keyChar < 0x41))
            {
                return true;
            }
            if ((0x5b <= keyChar) && (keyChar < 0x61))
            {
                return true;
            }
            if ((0x7b <= keyChar) && (keyChar < 0x7f))
            {
                return true;
            }
            return false;
        }
    }
}
