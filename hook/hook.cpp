// hook.cpp : DLL アプリケーション用にエクスポートされる関数を定義します。
//

#include "stdafx.h"
#include "hook.h"


#define TRACE(x)	TraceOut##x;


const UINT	COUNT_BANK = 2;
const UINT	COUNT_ITEM = 0x10000;
const DWORD	MS_KEYSEQ_INTERVAL_MAX = 6000;


struct KeyLogItem {
	uint32_t	msSpan;
	uint8_t		scanCode;
	uint8_t		modifiers;
	uint8_t		reserved1;
	uint8_t		reserved2;
};

HINSTANCE g_hModule;
HWND g_hWndMain;
uint32_t g_uMsgKey;
HHOOK g_hHook;
UINT	g_idxBankWrite;
UINT	g_idxBankRead;
LARGE_INTEGER	g_pfFreq;
LARGE_INTEGER	g_pfcKey;
uint8_t	g_modifiers;


KeyLogItem	g_keyLogItemBank[COUNT_BANK][COUNT_ITEM];
uint32_t	g_countItem[COUNT_BANK];


BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		g_hModule = hModule;
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}


void TraceOut(LPCTSTR format, ...)
{
	va_list args;
	va_start(args, format);
	TCHAR temp[256];
	wvsprintf(temp, format, args);
	OutputDebugString(temp);
}


void NotifyKey()
{
	if ((g_hWndMain != NULL) && (g_uMsgKey != 0))
	{
		PostMessage(g_hWndMain, g_uMsgKey, 0, 0);
	}
}


uint8_t	GetMidifierScanCode(const KBDLLHOOKSTRUCT* pKey)
{
	switch (pKey->scanCode)
	{
	case 0x1d:
		return ((pKey->flags & LLKHF_EXTENDED) == 0) ? 0x01 : 0x10;
	case 0x2a:
		return 0x02;		// LShift
	case 0x36:
		return 0x20;		// RShift
	case 0x38:
		return ((pKey->flags & LLKHF_EXTENDED) == 0) ? 0x04 : 0x40;
	default:
		break;
	}
	return 0x00;
}


bool UpdateModifiers(const KBDLLHOOKSTRUCT* pKey)
{
	auto modifierBit = GetMidifierScanCode(pKey);
//	TRACE((TEXT("scan:%04x modBit:%04x\r\n"), pKey->scanCode, modifierBit));
	if (modifierBit == 0x00)
	{
		return false;
	}
	if ((pKey->flags & LLKHF_UP) == 0)
	{
		g_modifiers |= modifierBit;
	}
	else
	{
		g_modifiers &= ~modifierBit;
	}
	return true;
}


void LogKey(const KBDLLHOOKSTRUCT* pKey)
{
	auto r = UpdateModifiers(pKey);
	if (r)
	{
		return;
	}

	const auto FLAGS_MASK = LLKHF_LOWER_IL_INJECTED | LLKHF_INJECTED | LLKHF_UP;
	const auto FLAGS_EXPECT = 0;
	if ((pKey->flags & FLAGS_MASK) != FLAGS_EXPECT)
	{
		return;
	}

	auto countItem = g_countItem[g_idxBankWrite];
	if (countItem >= COUNT_ITEM)
	{
		return;
	}
	auto pItem = &g_keyLogItemBank[g_idxBankWrite][countItem++];
	g_countItem[g_idxBankWrite] = countItem;

	auto pfcKeyPrev = g_pfcKey;
	QueryPerformanceCounter(&g_pfcKey);
	auto msSpan = (uint32_t)((g_pfcKey.QuadPart - pfcKeyPrev.QuadPart) * 1000 / g_pfFreq.QuadPart);

	TRACE((TEXT("scan:%02x\r\n"), pKey->scanCode))

	pItem->scanCode = static_cast<uint8_t>(pKey->scanCode);
	pItem->modifiers = g_modifiers;
	pItem->msSpan = msSpan;
	if (countItem != 1)
	{
		return;
	}
	NotifyKey();
}


LRESULT CALLBACK MyLowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION)
	{
		auto pKey = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);
		LogKey(pKey);
	}
	return CallNextHookEx(g_hHook, nCode, wParam, lParam);
}


extern "C" {

HOOK_API uint32_t WINAPI SetupHook(HWND hWndMain, uint32_t uMsgKey)
{
	if (g_hHook != NULL)
	{
		return ERROR_ALREADY_EXISTS;
	}

	g_hWndMain = hWndMain;
	g_uMsgKey = uMsgKey;
	g_idxBankWrite = 0;
	g_idxBankRead = 1;
	g_countItem[0] = 0;
	g_countItem[1] = 0;
	QueryPerformanceFrequency(&g_pfFreq);
	QueryPerformanceCounter(&g_pfcKey);

	g_hHook = SetWindowsHookEx(WH_KEYBOARD_LL, MyLowLevelKeyboardProc, g_hModule, 0);
	if (g_hHook == NULL)
	{
		auto err = GetLastError();
		return err;
	}
//	OutputDebugString(TEXT("SetubHook OK\r\n"));

	return 0;
}


HOOK_API void WINAPI CleanupHook()
{
	if (g_hHook == NULL)
	{
		return;
	}
	UnhookWindowsHookEx(g_hHook);
	g_hHook = NULL;
	g_hWndMain = NULL;
	g_uMsgKey = 0;
}


HOOK_API uint32_t WINAPI SwapBank()
{
	UINT idxTemp = g_idxBankWrite;
	g_idxBankWrite = g_idxBankRead;
	g_idxBankRead = idxTemp;
	g_countItem[g_idxBankWrite] = 0;

//	TCHAR temp[128];
//	wsprintf(temp, TEXT("%u %u\r\n"), g_idxBankWrite, g_idxBankRead);
//	OutputDebugString(temp);

	return g_countItem[g_idxBankRead];
}


HOOK_API void WINAPI GetKeyLogItem(uint32_t idx, KeyLogItem* out)
{
//	TCHAR temp[128];
//	wsprintf(temp, TEXT("read %u %u\r\n"), g_idxBankRead, idx);
//	OutputDebugString(temp);

	memset(out, 0, sizeof(KeyLogItem));
	if (idx >= g_countItem[g_idxBankRead])
	{
		return;
	}
	*out = g_keyLogItemBank[g_idxBankRead][idx];
}






} // extern "C"


